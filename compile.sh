#! /bin/bash

echo "compiling..."

gcc -o obj/q.o -c -g q.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -o obj/main.o -c -g main.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -o test obj/main.o obj/q.o

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

echo "compilation succesfull!! runnable called 'test'"
