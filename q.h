//q.h
#ifndef _Q_H
#define _Q_H

#include <stdio.h>
#include <stdlib.h>

struct q_node
{
    void *ptr_value;
    struct q_node *next;
    struct q_node *prev;
};

struct queue
{
    struct q_node *first;
    struct q_node *last;
    unsigned int size;
};

// returns 0 if queue is not empty, 1 otherwise.
unsigned int is_empty(struct queue *);
// returns a pointer to queue or NULL.
struct queue * new_queue();
// prints in console, every single node in queue.
void print_all(struct queue *);
// Returns 1 if the element is succesfully enqued to last, 0 otherwise.
unsigned int enq_lifo(struct queue *, void *);
// Returns 1 if the last element is succesfully dequed, 0 otherwise.
unsigned int deq_lifo(void *, struct queue *);
// deletes every single element in queue and frees it.
void clear(struct queue *);
#endif
