#! /bin/bash

echo "compiling..."

gcc -Wall -Werror -Wextra -pedantic -o obj/q_calc.o -c -g q_calc.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -Wall -Werror -Wextra -pedantic -o obj/main.o -c -g main.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -Wall -Werror -Wextra -pedantic -lm -o test obj/main.o obj/q_calc.o

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

echo "compilation finished succesfully!! executable named: 'test'"
