#include <time.h>
#include "q.h"
#include "q_calc.h"

int main()
{
    float lambda = 60;
    float mu = 66;
    float t = 0;
    float Po = 0;
    float Lq = 0;
    float Wq = 0;
    float W = 0;
    float L = 0;
    float Pw = 0;

    t = lambda/mu;
    Po = 1 - t;
    Lq = (t*t)/(1-t);
    Wq = Lq/lambda;
    W = Wq + (1/mu);
    L = lambda * W;
    Pw = 1 - Po;

    printf("[IN] lambda = %.4f, mu = %.4f\n", lambda, mu);
    printf("\t[RES] Factor trafico = %.4f\n", t);
    printf("\t[RES] Prob. Sist. no ocupado = %.4f\n", Po);
    printf("\t[RES] Largo promedio de la fila = %.4f\n", Lq);
    printf("\t[RES] Tiempo promedio de espera en la cola = %.4f\n", Wq);
    printf("\t[RES] Tiempo promedio de espera en el sist. = %.4f\n", W);
    printf("\t[RES] Cant. promedio de clientes en el sist. = %.4f\n", L);
    printf("\t[RES] Prob. un cliente deba esperar = %.4f\n", Pw);
    printf("\t[RES] n = 3; Pn = %.4f\n", get_Pn(lambda, mu, 3));
}
