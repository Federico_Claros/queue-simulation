//q_calc.c
#include "q_calc.h"

float get_Pn(float lambda, float mu, unsigned int n)
{
    return (pow((lambda/mu), n)*(1-(lambda/mu)));
}
