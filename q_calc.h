//q_calc.h

// This library calculates the performance indicators of a /M/M/1 queuing theory model
#ifndef _Q_CALC_H
#define _Q_CALC_H

#include <stdio.h>
#include <math.h>

// Receives a lambda, mu and n value.
// Returns the probability of n clients in the system.
float get_Pn(float, float, unsigned int);


#endif
