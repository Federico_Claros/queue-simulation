//q.c
#include "q.h"

unsigned int is_empty(struct queue *q)
{
    if(NULL == q)
      {
        return 1;
      }
    if(0 == q->size)
      {
        return 1;
      }
    return 0;
}

struct queue * new_queue()
{
  struct queue *q = (struct queue *) malloc(sizeof(struct queue));
    if(NULL == q)
      {
        return NULL;
      }
    q->size = 0;
    q->first = NULL;
    q->last = NULL;
    return q;
}

void print_all(struct queue *q)
{
    struct q_node *aux;
    unsigned int i = 0;

    if(NULL == q)
      {
        printf("[MSG] Queue is empty!!\n");
        return;
      }

    if(q->size < 1)
      {
        printf("[MSG] Queue is empty!!\n");
        return;
      }

    aux = q->first;
    printf("{prev, i, addr, next}\n");

    while(NULL != aux)
      {
        printf("{%p, %u, %p, %p}\n", aux->prev, i, aux, aux->next);
        aux = aux->next;
        i++;
      }
}

unsigned int enq_lifo(struct queue *q, void *value)
{
    struct q_node *e; // use malloc to create a new element.

    if(NULL == q)
      {
        return 0;
      }
    e = malloc(sizeof(struct q_node));

    if(NULL == e)
      {
        return 0;
      }
    e->ptr_value = value;

    if(is_empty(q))
      {
        q->first = e;
        q->last = e;
        q->size++;
        return 1;
      }
    q->first->prev = e;
    e->next = q->first;
    q->first = e;
    q->size++;
    return 1;
}

unsigned int deq_lifo(void *value, struct queue *q)
{
    if(NULL == q)
      {
        value = NULL;
        return 0;
      }
    if(0 == q->size)
      {
        value = NULL;
        return 0;
      }
    value = q->last->ptr_value; // GET ADDR OF LAST->VALUE!! AND PASS IT TO *VALUE

    if(1 == q->size)
      {
        q->last = NULL;
        free(q->first);
        q->first = NULL;
        q->size = 0;
        return 1;
      }
    struct q_node *aux = q->last->prev;
    aux->next = NULL;
    q->last->prev = NULL;
    free(q->last);
    q->last = aux;
    q->size --;
    return 1;
}

void clear(struct queue *q)
{
    void * aux;
    if(NULL == q)
      {
        return;
      }
    while(!is_empty(q))
      {
        deq_lifo(aux, q);
      }
    free(q);
    q = NULL;
}
